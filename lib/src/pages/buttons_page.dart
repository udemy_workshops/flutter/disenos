import 'dart:math' as math;
import 'dart:ui';

import 'package:flutter/material.dart';

enum _cellSide { left, right }

class ButtonsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Size tamagno = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(
        children: <Widget>[
          _fondoApp(tamagno),
          SafeArea(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _titulos(tamagno),
                  _botonesRedondeados(tamagno),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: _bottonNavigationBar(context),
    );
  }

  Widget _fondoApp(Size size) {
    final gradiente = Container(
      width: double.infinity,
      height: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Color.fromRGBO(52, 54, 101, 1.0),
            Color.fromRGBO(35, 37, 57, 1.0),
          ],
          begin: FractionalOffset(0.0, 0.6),
          end: FractionalOffset(0.0, 1.0),
        ),
      ),
    );

    final double alto = size.height * 0.85;
    final double ancho = size.width * 0.85;
    final double lado = alto > ancho ? ancho : alto;

    final cajaRosa = Positioned(
      top: -lado * 0.30,
      child: Transform.rotate(
        angle: -math.pi / 4.0,
        child: Container(
          height: lado,
          width: lado,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(lado * 0.20),
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(236, 98, 188, 1.0),
                Color.fromRGBO(241, 142, 172, 1.0),
              ],
            ),
          ),
        ),
      ),
    );

    return Stack(
      children: <Widget>[
        gradiente,
        cajaRosa,
      ],
    );
  }

  Widget _titulos(Size size) {
    final double alto = size.height * 0.80;
    final double ancho = size.width * 0.80;
    final double lado = alto > ancho ? ancho : alto;

    return Container(
      padding: EdgeInsets.all(20.0),
      width: lado,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Classify transaction',
            style: TextStyle(
              color: Colors.white,
              fontSize: 26.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            'Classify this transaction into a particular category',
            style: TextStyle(
              color: Colors.white,
              fontSize: 16.0,
            ),
          ),
        ],
      ),
    );
  }

  Widget _bottonNavigationBar(BuildContext context) {
    return new Theme(
      data: Theme.of(context).copyWith(
        canvasColor: Color.fromRGBO(55, 57, 84, 1.0),
        primaryColor: Colors.pinkAccent,
        textTheme: Theme.of(context).textTheme.copyWith(
              caption: TextStyle(
                color: Color.fromRGBO(116, 117, 152, 1.0),
              ),
            ),
      ),
      child: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(
              Icons.calendar_today,
              size: 30.0,
            ),
            title: Container(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.bubble_chart,
              size: 30.0,
            ),
            title: Container(),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.supervised_user_circle,
              size: 30.0,
            ),
            title: Container(),
          ),
        ],
      ),
    );
  }

  Widget _botonesRedondeados(Size size) {
    return Table(
      children: <TableRow>[
        TableRow(
          children: <Widget>[
            _botonRedondeado(
              size,
              _cellSide.left,
              Colors.blue,
              Icons.border_all,
              'General',
            ),
            _botonRedondeado(
              size,
              _cellSide.right,
              Colors.purple,
              Icons.directions_bus,
              'Transport',
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            _botonRedondeado(
              size,
              _cellSide.left,
              Colors.pink,
              Icons.shop,
              'Shopping',
            ),
            _botonRedondeado(
              size,
              _cellSide.right,
              Colors.orange,
              Icons.insert_drive_file,
              'Bills',
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            _botonRedondeado(
              size,
              _cellSide.left,
              Colors.blue,
              Icons.movie,
              'Entertainment',
            ),
            _botonRedondeado(
              size,
              _cellSide.right,
              Colors.green,
              Icons.local_grocery_store,
              'Grocery',
            ),
          ],
        ),
        TableRow(
          children: <Widget>[
            _botonRedondeado(
              size,
              _cellSide.left,
              Colors.red,
              Icons.collections,
              'Photos',
            ),
            _botonRedondeado(
              size,
              _cellSide.right,
              Colors.teal,
              Icons.help_outline,
              'General',
            ),
          ],
        ),
      ],
    );
  }

  Widget _botonRedondeado(
      Size size, _cellSide cellSide, Color color, IconData icon, String text) {
    final double lado = size.width * (0.50 - 0.02 * 3);

    final double margen = size.width * 0.02;

    return Container(
      height: lado,
      width: lado,
      margin: EdgeInsets.only(
        left: margen * (cellSide == _cellSide.left ? 2.0 : 1.0),
        right: margen * (cellSide == _cellSide.right ? 2.0 : 1.0),
        top: margen,
        bottom: margen,
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(62, 66, 107, 0.7),
        borderRadius: BorderRadius.circular(lado * 0.2),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(lado * 0.2),
        child: BackdropFilter(
          filter: ImageFilter.blur(
            sigmaX: 7.5,
            sigmaY: 7.5,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Container(
                height: lado * 0.40,
                width: lado * 0.40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(lado * 0.40),
                  gradient: LinearGradient(
                    colors: <Color>[
                      Colors.white,
                      color,
                    ],
                    begin: FractionalOffset(0.0, 0.0),
                    end: FractionalOffset(1.0, 1.0),
                  ),
                ),
                child: Icon(
                  icon,
                  color: Colors.white,
                  size: lado * 0.20,
                ),
              ),
              Text(
                text,
                style: TextStyle(
                  color: color,
                  fontSize: lado * 0.1,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
